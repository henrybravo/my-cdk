# my-cdk

A container with aws-cdk and it's dependent tools installed for **python** or **typescript** based cdk development. Instead of having to install the cdk onto your (many) environment(s) you can just build a container, push it to your registry and pull it from anywhere.

## aws-cdk
The AWS Cloud Development Kit [(AWS CDK)](https://docs.aws.amazon.com/cdk/latest/guide/home.html) is an open-source software development framework to define cloud infrastructure in code and provision it through AWS CloudFormation [more...](https://docs.aws.amazon.com/cdk/latest/guide/home.html)

## environment

This repository creates a Docker container that can be used as a local development environment for aws-cdk and **python** or **typescript** as the programming language with cdk. It is based on Ubuntu 20.04 LTS and has the following requirements installed:

* nvm v0.35.3

* node default v14.10.1

* cdk 1.63.0 (build 7a68125)

* pip 20.0.2 

* python3-venv

## usage

1. clone this repository and change cwd and mark scripts/*.sh executable

2. `docker build -t my-cdk:latest .`

3. `docker run -it my-cdk:latest`

4. add your awscli credentials in ˜/.aws/credentials (or volume mount it from local env)


## build containers with cdk
 
It is possible to use this image to also pull, create and/or push docker images from within the `my-cdk` container. In this case you have to run the `my-cdk` container on a Linux host.

This is an example of a use-case using [sysbox](https://github.com/nestybox/sysbox): "an open-source container runtime (runc), that enables Docker containers to act as virtual servers capable of running software such as Docker in them, easily and with proper isolation."

Currently the supported host OS includes Ubuntu only. Via the sysbox repository you can grab a .deb package to install in Ubuntu.

To start a `my-cdk` container that needs to support cdk python code that needs docker:

1. Start the system container

```$ docker run --runtime=sysbox-runc -it --hostname=my-cdk my-cdk:latest```

2. Swith to root

```$ sudo -i```

3. Start the inner Docker

```# dockerd > /var/log/dockerd.log 2>&1 &```

4. Verify Docker started correctly

```# docker info```

5. (optional) Start an inner container

```# docker run -it busybox```

Now that you have started and tested running an 'inner Docker container' you can deploy your cdk python code as usual.

## notes

- create a java equivalent image along with the rest of the cdk supported languages variants

- create code to deploy an preconfigured Ubuntu host with sysbox to build containers with cdk with this image