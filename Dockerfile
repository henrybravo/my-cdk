FROM ubuntu:20.04

# nvm v0.35.3
# node default v14.10.1
# cdk 1.63.0 (build 7a68125)

ADD scripts/bootstrap-root.sh /bootstrap-root.sh
RUN /bootstrap-root.sh

USER cdk
WORKDIR /home/cdk

ADD scripts/bootstrap-cdk.sh /home/cdk/bootstrap-cdk.sh
RUN /home/cdk/bootstrap-cdk.sh

CMD [ "bash" ]
