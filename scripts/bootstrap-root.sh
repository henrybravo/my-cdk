# set noninteractive installation
export DEBIAN_FRONTEND=noninteractive

# update
apt update -q

#install tzdata package
apt-get install -q -y --no-install-recommends apt-utils tzdata sudo git vim python3 python3-pip python3-venv awscli curl wget docker.io

# set your timezone
ln -fs /usr/share/zoneinfo/UTC /etc/localtime
dpkg-reconfigure --frontend noninteractive tzdata

# clean up
apt-get clean
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# sudoers
echo "%cdk ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# add user cdk
groupadd cdk 
useradd -m -g cdk -G docker -s /bin/bash cdk
